<?php

$installer = $this;

$installer->startSetup();

$customerEntityType = Mage::getModel('eav/entity_type')->loadByCode('customer');
$entityStoreConfig = Mage::getModel('eav/entity_store')->loadByEntityStore($customerEntityType->getId(), 0)
    ->setEntityTypeId(5)
    ->setStoreId(2)
    ->setIncrementPrefix('')
    ->setIncrementLastId('00')
    ->save();


$installer->endSetup();