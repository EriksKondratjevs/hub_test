<?php
/**
 * Created by PhpStorm.
 * User: erix2_000
 * Date: 13/07/2016
 * Time: 00:08
 */
$installer = $this;

$installer->startSetup();

$customerEntityType = Mage::getModel('eav/entity_type')->loadByCode('customer');
$entityStoreConfig = Mage::getModel('eav/entity_store')->loadByEntityStore($customerEntityType->getId(), 0)
    ->setEntityTypeId(5)
    ->setStoreId(2)
    ->setIncrementPrefix('5')
    ->setIncrementLastId('00')
    ->save();


$installer->endSetup();