<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('customer_entity'),
        'increment_prefix',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'length' => 1,
            'nullable' => true,
            'default' => 0,
            'comment' => 'i hope it s will work'
        )
    );

$installer->endSetup();